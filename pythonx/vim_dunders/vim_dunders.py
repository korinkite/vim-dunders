#!/usr/bin/env python
# -*- coding: utf-8 -*-

# IMPORT THIRD-PARTY LIBRARIES
import astroid
import vim

# IMPORT LOCAL LIBRARIES
from . import visitors_and_helpers as visitors
from .dunders import dunders


def _get_args_with_tabstops(method=None):
    args_with_tabstops = set()

    try:
        args_with_tabstops = _args = vim.eval('g:vim_dunders_args_tabstops')
    except vim.error:
        pass

    if method:
        try:
            _args = vim.eval('g:vim_dunders_{}_args_tabstops'.format(method.name))
        except vim.error:
            pass
        else:
            args_with_tabstops.update(set((arg.strip() for arg in _args if arg.strip())))

    return args_with_tabstops


def _get_dunder(name):
    methods = get_dunders()

    try:
        return methods[name]
    except KeyError:
        return


def is_in_class():
    '''bool: Check if the current row is inside of a class but not inside of a method.'''
    code = '\n'.join(vim.current.window.buffer)

    try:
        module = astroid.parse(code)
    except astroid.AstroidSyntaxError:
        return False

    (row, _) = vim.current.window.cursor
    current_row = row

    node = visitors.get_nearest_node(module, current_row)

    if not node:
        return False

    if isinstance(node, astroid.ClassDef):
        return True

    parent = node.parent

    while parent:
        is_inside_definition = current_row >= parent.fromlineno and current_row <= parent.tolineno

        if isinstance(parent, astroid.FunctionDef) and is_inside_definition:
            return False

        if isinstance(parent, astroid.ClassDef):
            return True

        parent = parent.parent

    return False


def override_args(method):
    try:
        args = vim.eval('g:vim_dunders_{}_args'.format(method.name))
    except vim.error:
        return

    args = [arg.strip() for arg in args if arg.strip()]
    method.args.args = args


def override_args_tabstops(method):
    args_with_tabstops = _get_args_with_tabstops(method)

    if not args_with_tabstops:
        return

    for index, arg in enumerate(method.args.args):
        if arg in args_with_tabstops:
            method.args.args[index] = dunders.Tabstop(arg)


def get_dunders():
    methods = dunders.get_dunders()
    for method in methods.values():
        override_args(method)
        override_args_tabstops(method)

    return methods


def get_ultisnips_dunder(name):
    method = _get_dunder(name)

    if not method:
        return ''

    return dunders.convert_to_ultisnips_dunder(method)
