#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''A series of classes and functions that help parse and "visit" Python source-code.'''

# IMPORT THIRD-PARTY LIBRARIES
import astroid


class AfterRowVisitor(object):

    '''A class that gets the nodes after a given row number.'''

    def __init__(self, row):
        '''Create the instance and store the given 1-based row number.'''
        super(AfterRowVisitor, self).__init__()
        self.row = row

    def _visit(self, node):
        '''Send all child nodes back to the user.

        Args:
            node (<astroid.Node>): The node to recursively get children from.

        Yields:
            <astroid.Node>: Any found child nodes.

        '''
        children = []

        for child in node.get_children():
            if child.fromlineno <= self.row:
                continue

            if child not in children:
                yield child
                children.append(child)

                for subchild in self.visit(child):
                    if subchild not in children:
                        yield child
                        children.append(subchild)

    def visit(self, node):
        '''list[<astroid.Node>]: All child nodes after the stored row number.'''
        return list(self._visit(node))

    def get_next(self, node):
        '''<astroid.Node> or NoneType: Get the very next node after the stored row.'''
        try:
            return next(self._visit(node))
        except StopIteration:
            return


def _get_cursor(lines):
    '''Find the user's cursor in the given text.

    Args:
        lines (iter[str]): The lines to check.

    Returns:
        tuple[int, int]:
            The found cursor. If no cursor is found, return the "not-found" value (-1, -1).

    '''
    for row, line in enumerate(lines):
        try:
            return (row, line.index('|x|'))
        except ValueError:
            pass

    return (-1, -1)


def is_valid_import_line(code, row):
    '''Check if the given `row` is in a position where it is "okay" to add an import.

    Args:
        code (str): The code to parse and check.
        row (int): A 1-based line number which represents the user's cursor.

    Returns:
        bool: If the user is in an import line.

    '''
    def get_nearest_relevant_node(node, row):
        try:
            node = get_nodes_until_row(node, row)[-1]
        except IndexError:
            return

        if isinstance(node, astroid.AssignName):
            return node.parent

        return node

    allowed_nodes = (
        # The start of a module definition
        astroid.Import,
        astroid.ImportFrom,

        # The start of a function definition
        astroid.Arguments,
    )

    try:
        module = astroid.parse(code)
    except astroid.AstroidSyntaxError:
        return False

    nearest_node = get_nearest_relevant_node(module, row)

    if not nearest_node:
        return True

    return isinstance(nearest_node, allowed_nodes)


def next_node(code, row):
    '''Find the next astroid child object after some line number.

    Args:
        code (str):
            The code to parse and get the next node of.
        row (int):
            A 1-based line number. This function will not return any child of
            `node` whose starting line is greater than this number.

    Returns:
        <astroid.Node> or NoneType: The found node, if any.

    '''
    try:
        module = astroid.parse(code)
    except astroid.AstroidSyntaxError:
        raise ValueError(('bad syntax in code', code))

    visitor = AfterRowVisitor(row)

    try:
        return visitor.get_next(module)
    except (IndexError, TypeError):
        return


def get_nearest_node(node, row):
    '''`astroid.Node` or NoneType: The closest node to the given `row`.'''
    try:
        return get_nodes_until_row(node, row)[-1]
    except IndexError:
        return


def get_nodes_until_row(node, row):
    '''Find every astroid node contained within the given node up to a row number.

    Args:
        node (<astroid.Node>):
            The node (presumably an <astroid.Module>) whose children will be returned.
        row (int):
            A 1-based line number. This function will not return any child of
            `node` whose starting line is greater than this number.

    Returns:
        list[<astroid.Node>]: All found nodes.

    '''
    children = []

    for child in node.get_children():
        if child.fromlineno >= row:
            break

        if child not in children:
            children.append(child)

            for subchild in get_nodes_until_row(child, row):
                if subchild not in children:
                    children.append(subchild)

    return children
