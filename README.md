# Python dunder-method snippets

Let Vim write your Python dunder (double-underscore) methods.


## Usage

Write the name of a dunder method (example: `init` or `iadd`) and then press 
your snippet-expansion key (For example. [TAB]) and let plug-in do the work.

https://asciinema.org/a/192306


## Requirements

This plug-in is an extension of [UltiSnips](https://github.com/SirVer/ultisnips)
and requires it in order to run.


## Installation

1. First, install UltiSnips if you have not installed it already.
2. Add this repository manually or use your preferred package manager and add
   the following line to your .vimrc.

Example (Using [vim-plug](https://github.com/junegunn/vim-plug)):
```
Plug 'https://bitbucket.org/korinkite/vim-dunder.git'
```

Then run `:PlugInstall` and restart Vim.


## Configuration Settings

### Overriding Arg Names
vim-dunders uses dynamic variables to let you customize the way your snippets
are created.

For example, the `__exit__` method has these arguments: 
['self', 'exec_type', 'exec_value', 'traceback']

This is based on Python's documentation. However, if you prefer different
argument names, you can customize them by adding this line to your ~/.vimc:

```
let g:vim_dunders_exit_args = ['self', 'foo', 'bar', 'fizz']
```

And then when the snippet expands, the result will be:

```
def __exit__(self, foo, bar, fizz):
    pass
```

Any dunder method can be modified this way. Just replace `{}` in `let g:vim_dunders_{}_args`
with whatever dunder-name that you want to modify.


### Overriding Tabstops
By default, vim-dunders doesn't add tabstops into the argument list of any
Python dunder method. If you want them, there are two ways to do it: By-Name or All

#### Overriding Tabstops - By-Name
vim-dunders defines `__getattr__` like this:

```
def __getattr__(self, name):
    ${1:pass}
```

Say you want the option to be able to change `name`.

```
let g:vim_dunders_getattr_args = ['self', 'attr']
```

This option will permanently change `name` into `attr` but if you want to have
the option to choose whether or not to change it, the best way is to add
another tabstop.

```
let g:vim_dunders_getattr_args_tabstops = ['name']
```

Now the generated snippet looks like this:

```
def __getattr__(self, ${1:name}):
    ${2:pass}
```

### Overriding Tabstops - All
If you have an argument that you want to add tabstops to for all dunder
methods, you can use `g:vim_dunders_args_tabstops`

Example:

Add a tabstop to every method that has "other" in its list of arguments:

```
let g:vim_dunders_args_tabstops = ['other']
```


### Summary

 g:vim_dunders_{}_args 			An override for that method's list of arguments.
 g:vim_dunders_{}_args_tabstops 	Lets you add additional tabstops to a method's args
